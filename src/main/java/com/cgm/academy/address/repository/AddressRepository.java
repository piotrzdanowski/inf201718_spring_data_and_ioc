package com.cgm.academy.address.repository;

import com.cgm.academy.address.domain.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Indexed;

@NoRepositoryBean
@Indexed
public interface AddressRepository extends CrudRepository<Address, Long> {
}
