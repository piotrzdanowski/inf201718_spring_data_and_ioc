package com.cgm.academy.address.domain;

public enum AddressType {
    REGISTERED, TEMPORARY, WORK
}
